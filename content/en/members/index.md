---
title: Members
featured_image: ''
description: Members of CEADS
omit_header_text: true
type: page
menu: main
---

# Current CEADS members

## Management
---
### Dr. Pepo Mena
{{< figure src="/images/Mena.jpg" width=150px class=fl >}}
Dr. Mena is a current post-doc with CEADS and has been a CEADS member since 2018. He completed both a M.S. in Nuclear Engineering and a PhD. in Applied Science & Engineering while working in CEADS. His research includes data science applications for operations management and nuclear science. He is currently teaching at Idaho State University in both management and computer science as well as performing research in nuclear cyber security as a postdoc.

## Graduate Students

---
### Samantha Ross
{{< figure src="/images/Ross.jpg" width=150px class=fl >}}
Samantha Ross joined CEADS November of 2022. She is a current graduate student in Idaho State University’s Computer Science program. She graduated with her Bachelor’s of Science in Computer Science from Idaho State University. Samantha’s CEADS research involves the security of machine learning models, currently focusing on inference attacks utilizing automated machine learning.


---
### Daniel Igbokwe
{{< figure src="/images/Igbokwe.jpg" width=150px class=fl >}}
Daniel is currently a computer science master's student at Idaho State University. He has completed his Bachelor’s in computer science. His research includes utilizing machine learning accelerated models and quantum chemistry to develop a predictive framework to understand multiphase interactions at solid/gas and solid/solid interfaces. 


--- 
### Avisha Jain
{{< figure src="/images/avishajain.png" width=150px class=fl >}}
Avisha is a software engineer with 5 years of experience working with reputable multinational companies. Her expertise lies in languages such as C#, Python, and SQL, and has a good command of technologies like ASP.NET Core, Azure, and AWS. Throughout my career, she has developed web-based platforms, performance testing tools, server simulators, and automated monitoring systems. Currently, she is pursuing a Master of Science in Computer Science from ISU, building upon her existing knowledge gained through a Bachelor of Engineering in Electronics.

https://www.linkedin.com/in/jainavisha/

---
### Autumn Clark
{{< figure src="/images/Clark.jpg" width=150px class=fl >}}
Autumn Clark graduated from Idaho State University (ISU) with an A.S. in Mathematics and a B.S. in Computer Science in Spring 2023. He is set to start pursuing a M.S. in Computer Science in Fall 2023 at ISU.

&nbsp;  
&nbsp;  

---
### Dunya Bahar
{{< figure src="/images/Dunya.jpg" width=150px class=fl >}}
Dunya is currently a researcher in the CEADS Cyber Nuclear Machine Learning group. They are set to graduate from ISU with an MS in Computer Science in May of 2024 and will pursue a career in AI/ML engineering.

https://www.linkedin.com/in/dunya-bahar-5646251b5/

---
### Alex Svancara
{{< figure src="/images/Svancara.jpg" width=150px class=fl >}}
Alex is currently working with CEADS on the quantum chemistry project. He graduated with his bachelors in Biochemistry from ISU in spring of 2023 and he is currently pursuing a masters degree in computer science at ISU.

&nbsp;

---
### Shah Md. Nehal Hasnaeen
{{< figure src="/images/nehal.jpeg" width=150px class=fl >}}
Hasnaeen is working with CEADS as a part of the Cyber Nuclear Research team starting summer 2023. His research interests include exploring the implementation of ML in electromagnetic signal analysis and cyber security. He will graduate with a Masters in Electrical and Computer Engineering at Idaho State University this summer and will start pursuing a PhD in Data Science from the University of Tennessee Knoxville on a graduate fellowship awarded by the Oak Ridge National Laboratory starting this fall. He has worked on refrigerated warehouse power optimization schemes and ultra-high frequency RFID electromagnetic fingerprinting using tree models.

---
### Brittany Grayson
{{< figure src="/images/Grayson.jpg" width=150px class=fl >}}
Brittany is a current PhD candidate in CEADS. Her proposal is titled: "A Machine Learning Approach to Fuel Load Optimization in the Advanced Test Reactor". She is currently a neutronics analyst at the Idaho National Laboratory.

https://www.linkedin.com/in/brittany-grayson-a40091122/

---
### Steven McDaniel
{{< figure src="/images/McDaniel.jpg" width=150px class=fl >}}
Steven McDaniel is a current PhD student in CEADS. He is a senior software engineer and patent agent with data science, machine learning and computer architecture experience.

https://www.linkedin.com/in/steven-mcdaniel-7321221/

---
### Chase Juneau
{{< figure src="/images/Juneau.jpg" width=150px class=fl >}}
Chase is a current PhD candidate in CEADS. He is currently an IT Tech at the Department of Revenue for the state of Montana. 

https://www.linkedin.com/in/chasem-juneau/
&nbsp;  
&nbsp;  

---
### Kyle Massey
{{< figure src="/images/Massey.jpg" width=150px class=fl >}}
Kyle is a MSNE student in CEADS. He is currently a senior nuclear engineer at the Idaho National Laboratory.

https://www.linkedin.com/in/kyle-massey-b9957396/
&nbsp;  
&nbsp;  

---
### Paul Gilbreath
{{< figure src="/images/Gilbreath.jpg" width=150px class=fl >}}
MSCS student

---


## Undergraduate Students

---
### Kallie McLaren
{{< figure src="/images/McLaren.jpg" width=150px class=fl >}}
Kallie is currently an undergraduate enrolled at Idaho State University. She is a senior and is dual majoring in Applied Mathematics and Statistics as well as obtaining a minor in Computer Science. She is currently planning to pursue a career in data science and/or cybersecurity. Her internships in CEADS have been the EXAsmr project as well as Nuclear Cybersecurity. 

---
### Eric Hill
{{< figure src="/images/Hill.jpg" width=150px class=fl >}}
Eric Hill is an undergraduate at Idaho State University. He is a senior in the Mechanical Engineering program, and is also pursuing a Computer Science minor and a Math minor. He is working on the CEADS cyber nuke AI project. He is interested in furthering his knowledge of both engineering and computer science.

---
### Craig Price
{{< figure src="/images/Price.jpg" width=150px class=fl >}}
Craig is currently an undergraduate student at Idaho State University. He is a senior studying Computer Science.

&nbsp;  
&nbsp;  
&nbsp;  

---
### Sindi Banda
{{< figure src="/images/Banda.jpg" width=150px class=fl >}}
Sindi is a senior at Idaho State University, majoring in Business Informatics. Alongside his studies, he has also worked as an AR/VR developer in the Department of Science and Civil Engineering at Idaho State University as a career path intern. This summer, he will be joining the CEADS team as a research intern, focusing on Cyber Nuclear Machine Learning.

https://www.linkedin.com/in/sindi-banda/

---
### Zac Schepis
{{< figure src="/images/Zac.jpg" width=150px class=fl >}}
Zac at the time of writing this, is an undergraduate student at ISU. He is majoring in Computer Science and is working on a cyber security certificate on the side.

https://www.linkedin.com/in/zackary-schepis-9ab9b1218/

---
### Pratham Khanal
{{< figure src="/images/Pratham.jpg" width=150px class=fl >}}
I am Pratham Khanal, a CS Undergraduate student from Nepal. My area of career interest includes Machine Learning in Business and Trading. I am doing research on SBOM - Software Bill of Materials exploiting the vulnerability with software supply chain security. This research will not only help me work on Machine Learning and Computational Software but also implement the structure of SBOMs on System Software for corporate companies.

https://www.linkedin.com/in/pratham-khanal-515499238/


---
### Ryan Bartel
{{< figure src="/images/Ryan.jpg" width=150px class=fl >}}
Ryan is a computer science undergraduate student at Idaho State University. He is interested in data science and machine learning and is currently working on the SBOMs project.

&nbsp;  
&nbsp;

---
### Gabriel Ory
{{< figure src="/images/Gabriel.jpg" width=150px class=fl >}}
Gabriel is an undergraduate student at Idaho State University majoring in Computer Science and minoring in Applied Mathematics. He is a sophomore who joined CEADS on the Cyber Nuclear Machine Learning project in the summer of 2023. Additionally, he has been a math tutor at ISU since the spring of 2022, with experience tutoring in the TAP Center and for the START and Adult Education programs. In the future, he hopes to pursue a career in data science or game development.

---
### Hyunsu Kim
{{< figure src="/images/Kim.jpeg" width=150px class=fl >}}
Hyunsu Kim is an undergraduate student at Idaho State University. He is majoring in computer science and senior. He is doing an internship at CEADS as a member of the Cyber Nuclear Machine Learning team.

&nbsp;  

---
### Gunnar Babicz
{{< figure src="/images/Gunnar.jpeg" width=150px class=fl >}}
Gunnar is an undergraduate student in Computer Science. He is currently working on the Nuclear Cybersecurity project. 

https://www.linkedin.com/in/gunnarbabicz/

&nbsp;  

---
### Sansar Khanal
{{< figure src="/images/Khanal.jpg" width=150px class=fl >}}
My name is Sansar Kharal. I am from Nepal. I am majoring in Computer Science at Idaho State University. I am in my junior year right now.  I am currently working on the Cyber Nuclear Project this summer as an Intern for Dr. Kerby and Dr. Mena. Right now. I am planning to pursue my future career in Software Design or Artificial Intelligence. 

---
### Grant Madson
{{< figure src="/images/Grant.jpg" width=150px class=fl >}}
Grant is a Computer Science major at ISU and will also be obtaining a certificate in cybersecurity. He is working on the quantum chemistry ML project at CEADS.

&nbsp;  
&nbsp;  
&nbsp;  

---
### Hyunse Seo
{{< figure src="/images/Seo.jpg" width=150px class=fl >}}
Hello, I'm Hyunse Seo, an enthusiastic individual from Korea, currently pursuing my studies at Idaho State University. I am excited to participate in a beautiful summer project at CEADS, where I participate in the Software Bill of Materials (SBOM) With passionate group members.  I am constantly interested in exploring and mastering various modern web frameworks.

---
### Saugat Acharya
{{< figure src="/images/Saugat.jpg" width=150px class=fl >}}
I am Saugat Acharya. I am an undergraduate Computer Science student at Idaho State University. I am currently working on the Quantum Chemistry Machine Learning project at CEADS. 

https://www.linkedin.com/in/saugat-acharya10

&nbsp;  
&nbsp;  


# Past CEADS members
---

### Shovan Chowdhury
{{< figure src="/images/Chowdhury.jpg" width=150px class=fl >}}
Shovan is a past graduate student in CEADS, studying machine learning applications in Li-ion battery performance. He has MS Mechanical Engineering at ISU; his thesis is titled: "Artificial Intelligence Based Li-ion Battery Diagnostic Platform." He is currently a PhD student at the University of Tennessee - Knoxville, on an Oak Ridge National Laboratory graduate fellowship. 

https://www.linkedin.com/in/shovan-chowdhury/

---
### Patience Lamb, past CEADS student
{{< figure src="/images/Lamb.jpg" width=150px class=fl >}}
Patience Lamb graduated Idaho State University in 2022 with a Bachelors of Science in Nuclear Engineering and Computer Science. As a part of the CEADS group, Patience helped with the development of Functional Expansion Tallies within Shift/SCALE for the ExaSMR project. Currently, she is attending the Georgia Institute of Technology for her PhD in Nuclear and Radiological Engineering, focusing on nuclear cybersecurity.

https://www.linkedin.com/in/patience-lamb/

---
### Emily Elzinga
Emily is a current undergraduate at ISU as a CS and BS Physics dual major. She has worked on the cyber nuclear AI project, as well as ExaSMR.

---
### Dr. Ryan Stewart
{{< figure src="/images/Stewart.jpg" width=150px class=fl >}}
Dr. Stewart is a past graduate student in CEADS, working in the ExaSMR project. He is currently a nuclear engineer at the Idaho National Laboratory.

https://www.linkedin.com/in/ryanstewartnuclear/
&nbsp;  
&nbsp;  

---
### Aaron Johnson
{{< figure src="/images/Johnson.jpg" width=150px class=fl >}}
Aaron is a past CEADS undergraduate and graduate student, working in the ExaSMR project. He completed a BSCS at ISU and is currently a tech at College of Eastern Idaho.

https://www.linkedin.com/in/aaron-p-johnson/

---
### Katherine Wilsdon
{{< figure src="/images/Wilsdon.jpg" width=150px class=fl >}}
Katherine is a past CEADS undergraduate student, working in the ExaSMR project. She completed a BSCS at ISU and is currently a data scientist and software engineer at the Idaho National Laboratory.

https://www.linkedin.com/in/katherine-wilsdon/

---
### Dr. Brycen Wendt
{{< figure src="/images/Wendt.jpg" width=150px class=fl >}}
Brycen is a past CEADS PhD student. His dissertation is titled: "Functional Expansions Methods: Optimizations, Characterizations, and Multiphysics Practices." He is currently an R&D engineer at ANSYS, Inc.

https://www.linkedin.com/in/brycen-wendt/

---
### Steven Wacker
{{< figure src="/images/Wacker.jpg" width=150px class=fl >}}
BSNE

---

*... and more...*


